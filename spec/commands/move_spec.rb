require 'spec_helper'

RSpec.describe Commands::Move do
  let(:table) { Table.new }

  describe 'move' do
    it 'should not move if robot has not been placed' do
      expect(described_class.new(Robot.new, table).go).to eql nil
    end

    it 'should be able to move NORTH' do
      robot = Robot.new(1, 1, 'NORTH')
      described_class.new(robot, table).move
      expect(robot.orientation).to eql 'NORTH'
      expect(robot.x_position).to eql 1
      expect(robot.y_position).to eql 2
    end

    it 'should be able to move south' do
      robot = Robot.new(1, 1, 'SOUTH')
      described_class.new(robot, table).move
      expect(robot.orientation).to eql 'SOUTH'
      expect(robot.x_position).to eql 1
      expect(robot.y_position).to eql 0
    end

    it 'should be able to move east' do
      robot = Robot.new(1, 1, 'EAST')
      described_class.new(robot, table).move
      expect(robot.orientation).to eql 'EAST'
      expect(robot.x_position).to eql 2
      expect(robot.y_position).to eql 1
    end

    it 'should be able to move west' do
      robot = Robot.new(1, 1, 'WEST')
      described_class.new(robot, table).move
      expect(robot.orientation).to eql 'WEST'
      expect(robot.x_position).to eql 0
      expect(robot.y_position).to eql 1
    end
  end

  describe 'can_move' do
    it 'should return false when on southern edge and facing south' do
      robot = Robot.new(3, 0, 'SOUTH')
      expect(described_class.new(robot, table).can_move?).to eql false
    end

    it 'should return false when on northern edge and facing north' do
      robot = Robot.new(2, 5, 'NORTH')
      expect(described_class.new(robot, table).can_move?).to eql false
    end

    it 'should return false when on eastern edge and facing east' do
      robot = Robot.new(5, 1, 'EAST')
      expect(described_class.new(robot, table).can_move?).to eql false
    end

    it 'should return false when on western edge and facing west' do
      robot = Robot.new(0, 3, 'WEST')
      expect(described_class.new(robot, table).can_move?).to eql false
    end

    it 'should return true when not on edge' do
      Robot::COMPASS.each do |direction|
        robot = Robot.new(1, 1, direction)
        expect(described_class.new(robot, table).can_move?).to eql true
      end
    end
  end
end
