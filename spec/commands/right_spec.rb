require 'spec_helper'

RSpec.describe Commands::Right do
  let(:robot) { Robot.new }

  describe 'go' do

    it 'should not turn right if robot has not been placed' do
      described_class.new(robot).go
      expect(robot.orientation).to eql nil
      expect(robot.x_position).to eql nil
      expect(robot.y_position).to eql nil
    end

    it 'should turn right if robot is placed' do
      robot.orientation = 'EAST'
      robot.x_position = 2
      robot.y_position = 2
      described_class.new(robot).go
      expect(robot.orientation).to eql 'SOUTH'
      expect(robot.x_position).to eql 2
      expect(robot.y_position).to eql 2
    end
  end
end
