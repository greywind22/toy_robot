require 'spec_helper'

RSpec.describe Commands::Place do
  let(:table) { Table.new }
  let(:robot) { Robot.new }

  describe 'go' do

    it 'should do nothing if arguments are invalid' do
      described_class.new(robot, table, '1,2,INVALID').go
      expect(robot.orientation).to eql nil
      expect(robot.x_position).to eql nil
      expect(robot.y_position).to eql nil
    end

    it 'should place robot if within the table' do
      described_class.new(robot, table, '1,2,EAST').go
      expect(robot.orientation).to eql 'EAST'
      expect(robot.x_position).to eql 1
      expect(robot.y_position).to eql 2
    end

    it 'should not place robot if out of bounds' do
      described_class.new(robot, table, '6,2,WEST').go
      expect(robot.orientation).to eql nil
      expect(robot.x_position).to eql nil
      expect(robot.y_position).to eql nil
    end
  end
end
