require 'spec_helper'

RSpec.describe Commands::Left do
  let(:robot) { Robot.new }

  describe 'go' do

    it 'should not turn left if robot has not been placed' do
      described_class.new(robot).go
      expect(robot.orientation).to eql nil
      expect(robot.x_position).to eql nil
      expect(robot.y_position).to eql nil
    end

    it 'should turn left if robot is placed' do
      robot.orientation = 'NORTH'
      robot.x_position = 1
      robot.y_position = 1
      described_class.new(robot).go
      expect(robot.orientation).to eql 'WEST'
      expect(robot.x_position).to eql 1
      expect(robot.y_position).to eql 1
    end
  end
end
