require 'spec_helper'

RSpec.describe Robot do
  describe 'placed?' do
    it 'should return false if robot has not been placed' do
      expect(described_class.new.placed?).to eql false
    end

    it 'should return true if robot has been placed' do
      expect(described_class.new(1, 5, 'WEST').placed?).to eql true
    end
  end
end
