require 'spec_helper'

RSpec.describe CommandDispatcher do
  let(:table) { Table.new }
  let(:robot) { Robot.new(1, 1, 'NORTH') }

  it 'should call left command' do
    expect(described_class.new(robot, table).execute('LEFT')).to be_an_instance_of Commands::Left
  end

  it 'should call right command' do
    expect(described_class.new(robot, table).execute('RIGHT')).to be_an_instance_of Commands::Right
  end

  it 'should call move command' do
    expect(described_class.new(robot, table).execute('MOVE')).to be_an_instance_of Commands::Move
  end

  it 'should call place command' do
    expect(described_class.new(robot, table).execute('PLACE')).to be_an_instance_of Commands::Place
  end

  it 'should do nothing if command is not recognised' do
    expect(described_class.new(robot, table).execute('Go Skynet')).to be nil
  end
end
