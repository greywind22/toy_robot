require_relative 'table'
require_relative 'robot'
require_relative 'command_dispatcher'

table = Table.new
robot = Robot.new
command_dispatcher = CommandDispatcher.new(robot, table)

puts 'Toy Robot Simulator'
puts 'Valid Commands -'
puts 'PLACE X,Y,F'
puts 'MOVE'
puts 'LEFT'
puts 'RIGHT'
puts "REPORT\n\n"
puts "Exit to stop simulation\n\n"

loop do
  puts 'Please input a command: '

  input = STDIN.gets

  if input.chomp.casecmp('exit').zero?
    puts 'Bye Bye Bye!'
    break
  end

  command = command_dispatcher.execute(input)
  command.go unless command.nil?
end
