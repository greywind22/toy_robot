require_relative 'commands/left'
require_relative 'commands/right'
require_relative 'commands/move'
require_relative 'commands/place'
require_relative 'commands/report'

class CommandDispatcher
  COMMANDS = %w[LEFT RIGHT MOVE PLACE REPORT].freeze

  def initialize(robot, table)
    @robot = robot
    @table = table
  end

  def execute(input)
    return if input.nil?
    command, args = input.chomp.split(' ')
    command = command.upcase

    if !COMMANDS.include?(command)
      puts 'Command not recognised... Discarding'
    end

    case command
    when 'LEFT'
      Commands::Left.new(@robot)
    when 'RIGHT'
      Commands::Right.new(@robot)
    when 'MOVE'
      Commands::Move.new(@robot, @table)
    when 'PLACE'
      Commands::Place.new(@robot, @table, args)
    when 'REPORT'
      Commands::Report.new(@robot)
    end
  end
end
