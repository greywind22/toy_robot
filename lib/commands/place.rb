module Commands
  class Place

    attr_accessor :robot, :table, :args

    def initialize(robot, table, args)
      @robot = robot
      @table = table
      @args = args
    end

    def go
      if args.nil? || args.split(',').size != 3
        puts 'Arguments not valid'
        return
      end
      x, y, orientation = args.split(',')
      x = x.to_i
      y = y.to_i

      if x > table.size || x < 0 ||
         y > table.size || y < 0 ||
         !Robot::COMPASS.include?(orientation.upcase)
        return
      end

      robot.x_position = x
      robot.y_position = y
      robot.orientation = orientation
    end
  end
end
