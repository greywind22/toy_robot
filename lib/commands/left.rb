module Commands
  class Left

    attr_accessor :robot

    def initialize(robot)
      @robot = robot
    end

    def go
      return unless robot.placed?
      current_idx = Robot::COMPASS.index(robot.orientation)
      new_idx = current_idx.zero? ? Robot::COMPASS.size - 1 : current_idx - 1
      robot.orientation = Robot::COMPASS[new_idx]
    end
  end
end
