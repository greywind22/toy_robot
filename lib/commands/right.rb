module Commands
  class Right
    attr_accessor :robot

    def initialize(robot)
      @robot = robot
    end

    def go
      return unless robot.placed?
      current_idx = Robot::COMPASS.index(robot.orientation)
      new_idx = current_idx == (Robot::COMPASS.size - 1) ? 0 : current_idx + 1
      robot.orientation = Robot::COMPASS[new_idx]
    end
  end
end
