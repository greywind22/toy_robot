module Commands
  class Report

    attr_accessor :robot

    def initialize(robot)
      @robot = robot
    end

    def go
      if robot.placed?
        puts "X: #{robot.x_position}, Y: #{robot.y_position}, Orientation: #{robot.orientation}"
      else
        puts 'Robot has not been positioned yet'
      end
    end
  end
end
