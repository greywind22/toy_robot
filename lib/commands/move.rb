module Commands
  class Move

    attr_accessor :robot, :table

    def initialize(robot, table)
      @robot = robot
      @table = table
    end

    def go
      return unless robot.placed?
      return unless can_move?
      move
    end

    def can_move?
      case robot.orientation.upcase
      when 'NORTH'
        robot.y_position < table.size
      when 'EAST'
        robot.x_position < table.size
      when 'SOUTH'
        robot.y_position > 0
      when 'WEST'
        robot.x_position > 0
      end
    end

    def move
      case robot.orientation.upcase
      when 'NORTH'
        robot.y_position += 1
      when 'EAST'
        robot.x_position += 1
      when 'SOUTH'
        robot.y_position -= 1
      when 'WEST'
        robot.x_position -= 1
      end
    end
  end
end
