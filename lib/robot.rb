class Robot

  COMPASS = %w[NORTH EAST SOUTH WEST].freeze

  attr_accessor :x_position, :y_position, :orientation
  def initialize(x_pos = nil, y_pos = nil, orientation = nil)
    @x_position = x_pos
    @y_position = y_pos
    @orientation = orientation
  end

  def placed?
    !x_position.nil? && !y_position.nil? && !orientation.nil?
  end
end
